# Objective

Make a program that uses a file system filter driver to detect when a file, that has a name containing a given string, is opened for reading.

# Building the development environment

- Download a Windows 10 ISO and create test machine image using VMWare.  Install Windows updates, SDK and WDK (1903) and set-up kernel debugging.

- Provision the test image from within VS 2019. Issues resolved:
	- Errors about missing paths.  This was resolved using a fix from the [MSDN forums](https://social.msdn.microsoft.com/Forums/en-US/31838202-0086-41b4-b424-72e7c121da4a/hardware-development-kits-for-windows-10-version-1903?forum=wdk).

- Read the [Filesystems driver design guide](https://docs.microsoft.com/en-gb/windows-hardware/drivers/ifs/) to see where to start on the driver side.

- Microsoft provide samples and the [MiniSpy Filesystem Minifilter Driver](https://docs.microsoft.com/en-us/samples/microsoft/windows-driver-samples/minispy-file-system-minifilter-driver/) is very close to what's needed. It even includes the user-mode program that can communicate bi-directionally with the minifilter. Repo is [here](https://github.com/microsoft/Windows-driver-samples).

- Copy the `minispy` project and get it building. Issues resolved:
	- Missing VS components (WDK and Spectre-migated libraries).  Resolved by adding them from *Visual Studio Installer*.
	- Move the `minispy.inf` file into the `filter` directory so it gets processed during the build, allowing *deploy* to work.
	- Errors detected in `minispy.inf` during build.  Solution found on [OSR forums](https://community.osr.com/discussion/291286/wdk10-stampinf-returns-error-1420-defaultinstall-based-inf-cannot-be-processed-as-primitive).
	- Remove references to `minispy.exe` from `minispy.inf` as I cannot see how to reference the `.exe` properly.  Instead I will make the *user* program use *Remote Debugging* to deploy it to the test image.

- Add a breakpoint in `DriverEntry()` and *Debug* the minifilter in VS.  Driver builds and deploys, including removing the old driver using *TAEF*.  I don't know what this is actually doing other than deploying the driver binaries to the `C:\DriverTest\Drivers` directory on the test image.

- On the test image:
```
> cd \DriverTest\Drivers
> rundll32 setupapi.dll,InstallHinfSection DefaultInstall 132 .\minispy.inf
> fltmc load minispy
```

- Breakpoint hit!  I can debug the driver!

# Minifilter changes

- Add a new command message type (`SetMiniSpyFilenameFragment`) that allows the user program to set the filename fragment we want to filter on.
- Remove the interception of all operation types other than `IRP_MJ_CREATE`.
- Chop out most of the functionality in `SpyPreOperationCallback()` so that it only filters when a filename fragment has been configured, and when the operation is an open operation in read mode.  I also chopped out some backwards-compat code to clean up the method a bit.

# User program changes

- Restructure `main()` to avoid `goto` statements.
- Remove the interaction with the user; create communication port and logging thread and then wait for the logging thread to terminate.
- Add *Console Ctrl Handler* so that *Ctrl-C* in intercepted and sets the *log-thread-terminate boolean*.


# What I didn't do

- I didn't convert the user-side program to C++ as there was no immediate reason to do so, other than to interact with other C++ libraries.
- I haven't run the program (yet) to test if it actually works.
