/*++

Copyright (c) 1989-2002  Microsoft Corporation

Module Name:

    mspyUser.c

Abstract:

    This file contains the implementation for the main function of the
    user application piece of MiniSpy.  This function is responsible for
    controlling the command mode available to the user to control the
    kernel mode driver.

Environment:

    User mode

--*/

#include <DriverSpecs.h>
_Analysis_mode_(_Analysis_code_type_user_code_)

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <assert.h>
#include "mspyLog.h"
#include <strsafe.h>

#define SUCCESS              0
#define USAGE_ERROR          1
#define EXIT_INTERPRETER     2
#define EXIT_PROGRAM         4

#define INTERPRETER_EXIT_COMMAND1 "go"
#define INTERPRETER_EXIT_COMMAND2 "g"
#define PROGRAM_EXIT_COMMAND      "exit"
#define CMDLINE_SIZE              256
#define NUM_PARAMS                40

#define MINISPY_NAME            L"MiniSpy"
#define ATTACH_VOLUME           L"C:\\"
#define INSTANCE_NAME           L"TheInstance"

//
// Had to make the logging context global so the CtrlHandler() can signal the logging
// thread to stop.  The ConsoleCtrlHandler callback function doesn't accept a "context",
// making it a very poor callback mechanism.
//
static LOG_CONTEXT context;

// Forward
static BOOL SetFilenameFragment(PCWCHAR filenameFragment);
static BOOL WINAPI CtrlHandler(DWORD fdwCtrlType);
static VOID DisplayError(_In_ DWORD Code);

int _cdecl
main (
    _In_ int argc,
    _In_reads_(argc) char *argv[]
    )
/*++

Routine Description:

    Main routine for minispy

Arguments:

Return Value:

--*/
{
    HRESULT hResult = S_OK;
    DWORD result;
    ULONG threadId;
    HANDLE thread = INVALID_HANDLE_VALUE;
    WCHAR filenameFragment[1024];
    int len;

    ZeroMemory(&context, sizeof(context));

    //
    // Process command line arguments
    //

    if (argc != 2) {
        fprintf(stderr, "Usage: minispy filename_fragment\n");
        return 1;
    }

    len = MultiByteToWideChar(CP_UTF8, 0, argv[1], -1, filenameFragment, sizeof(filenameFragment) / sizeof(WCHAR) - 1);
    filenameFragment[len] = (WCHAR)0;

    //
    //  Open the port that is used to talk to
    //  MiniSpy.
    //

    printf( "Connecting to filter's port...\n" );

    hResult = FilterConnectCommunicationPort( MINISPY_PORT_NAME,
                                              0,
                                              NULL,
                                              0,
                                              NULL,
                                              &context.Port );

    if (SUCCEEDED(hResult)) {

        //
        // Initialize the fields of the LOG_CONTEXT
        //

        context.ShutDown = CreateSemaphore( NULL,
                                            0,
                                            1,
                                            L"MiniSpy shut down" );
        if (context.ShutDown != INVALID_HANDLE_VALUE) {
            //
            // Create the thread to read the log records that are gathered
            // by MiniSpy.sys.
            //
            printf( "Creating logging thread...\n" );
            thread = CreateThread( NULL,
                                   0,
                                   RetrieveLogRecords,
                                   (LPVOID)&context,
                                   0,
                                   &threadId);

            if (thread != INVALID_HANDLE_VALUE) {
    
                //
                // Attach to C: (hard-coded for now)
                //

                printf("Attaching to %ws", ATTACH_VOLUME);

                hResult = FilterAttach(MINISPY_NAME,
                    ATTACH_VOLUME,
                    INSTANCE_NAME,
                    sizeof(INSTANCE_NAME),
                    NULL);

                if (SUCCEEDED(hResult)) {
                    //
                    // Set the filename fragment passed in via the command line
                    //
                    if (SetFilenameFragment(filenameFragment)) {

                        printf("Press Ctrl-C to exit...\n");

                    } else {

                        // Tell the logging thread to exit polietly
                        context.CleaningUp = TRUE;
                    }

                } else {

                    printf("Could not attach to device: 0x%08x\n", hResult);
                    DisplayError(hResult);

                    // Tell the logging thread to exit polietly
                    context.CleaningUp = TRUE;
                }

                //
                // Wait for logging thread to shutdown.
                // 1) context.CleaningUp set TRUE by ConsoleCtrlHandler callback (or above if attach fails)
                // 2) Logging thread sees context.CleaninUp and exits its loop.
                // 3) Logging thread releases semaphore
                //

                WaitForSingleObject(context.ShutDown, INFINITE);

                hResult = FilterDetach(MINISPY_NAME,
                                       ATTACH_VOLUME,
                                       INSTANCE_NAME);

                if (FAILED(hResult)) {
                    // Don't care; silence compiler.
                }

                CloseHandle(thread);

            } else {
                result = GetLastError();
                printf( "Could not create logging thread: %d\n", result );
                DisplayError( result );
            }

            CloseHandle(context.ShutDown);

        } else {

            result = GetLastError();
            printf("Could not create semaphore: %d\n", result);
            DisplayError(result);

        }

        CloseHandle(context.Port);
 
    } else {

        printf("Could not connect to filter: 0x%08x\n", hResult);
        DisplayError(hResult);
    }

    return 0;
}

static BOOL SetFilenameFragment(PCWCHAR filenameFragment)
{
    BOOL retval = FALSE;
    size_t strLen;
    PCOMMAND_MESSAGE command;
    NTSTATUS statusReply;
    DWORD statusReplySize = 0;
    HRESULT hr;

    strLen = filenameFragment != NULL ? wcslen(filenameFragment) : 0;
    command = (PCOMMAND_MESSAGE)LocalAlloc(LPTR, sizeof(COMMAND_MESSAGE) + strLen * sizeof(WCHAR));
    if (command != NULL) {
        command->Command = SetMiniSpyFilenameFragment;
        CopyMemory(command->Data, filenameFragment, strLen * sizeof(WCHAR));
        hr = FilterSendMessage(context.Port,
                               command,
                               (DWORD)(sizeof(COMMAND_MESSAGE) + strLen * sizeof(WCHAR)),
                               &statusReply,
                               sizeof(statusReply),
                               &statusReplySize);
        if (SUCCEEDED(hr)) {
            if (statusReplySize == sizeof(statusReply) && SUCCEEDED(statusReply)) {
                retval = TRUE;
            } else {
                printf("MiniSpy refused filename fragment: 0x%08x\n", statusReply);
                DisplayError(statusReply);
            }
        } else {
            printf("Failed to set filename fragment: 0x%08x\n", hr);
            DisplayError(hr);
        }
        LocalFree(command);
    } else {
        printf("Failed to allocate memory for SetMiniSpyFilenameFragment command: 0x%08x\n", GetLastError());
    }

    return retval;
}

static BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
{
    UNREFERENCED_PARAMETER(fdwCtrlType);

    context.CleaningUp = TRUE;
    return TRUE;

}

static VOID DisplayError(_In_ DWORD Code)

/*++

Routine Description:

   This routine will display an error message based off of the Win32 error
   code that is passed in. This allows the user to see an understandable
   error message instead of just the code.

Arguments:

   Code - The error code to be translated.

Return Value:

   None.

--*/

{
    WCHAR buffer[MAX_PATH] = { 0 };
    DWORD count;
    HMODULE module = NULL;
    HRESULT status;

    count = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        Code,
        0,
        buffer,
        sizeof(buffer) / sizeof(WCHAR),
        NULL);


    if (count == 0) {

        count = GetSystemDirectory(buffer,
            sizeof(buffer) / sizeof(WCHAR));

        if (count == 0 || count > sizeof(buffer) / sizeof(WCHAR)) {

            //
            //  In practice we expect buffer to be large enough to hold the 
            //  system directory path. 
            //

            printf("    Could not translate error: %d\n", Code);
            return;
        }


        status = StringCchCat(buffer,
            sizeof(buffer) / sizeof(WCHAR),
            L"\\fltlib.dll");

        if (status != S_OK) {

            printf("    Could not translate error: %d\n", Code);
            return;
        }

        module = LoadLibraryExW(buffer, NULL, LOAD_LIBRARY_AS_DATAFILE);

        //
        //  Translate the Win32 error code into a useful message.
        //

        count = FormatMessage(FORMAT_MESSAGE_FROM_HMODULE,
            module,
            Code,
            0,
            buffer,
            sizeof(buffer) / sizeof(WCHAR),
            NULL);

        if (module != NULL) {

            FreeLibrary(module);
        }

        //
        //  If we still couldn't resolve the message, generate a string
        //

        if (count == 0) {

            printf("    Could not translate error: %d\n", Code);
            return;
        }
    }

    //
    //  Display the translated error.
    //

    printf("    %ws\n", buffer);
}
